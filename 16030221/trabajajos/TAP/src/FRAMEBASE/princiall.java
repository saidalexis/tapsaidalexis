package FRAMEBASE;
import FRAMEBASE.space;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *@author
 *  Yesenia Lizbeth Guerrero Garcia
 *Nancy Yessenia Garcia Mayorga
 * Yariseth Lizeth Guerrero Garcia
*/

public class princiall {
  
    private ImageIcon ImagenMenu = new ImageIcon("/FRAMEBASE/espacio.jpg");
    private JFrame Ventana = null;
  
    public princiall(){
        CrearInterfaz();
    }
  
    private void CrearInterfaz(){
      
        //Crea el Frame
        Ventana = new JFrame("Space Invaders -- Menú Principal");
        Ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Ventana.setVisible(true);
        Ventana.setSize(603,447);
        Ventana.setResizable(false);
        Ventana.setLayout(null);
      
        //Añade fondo al frame
        JLabel Fondo = new JLabel(ImagenMenu);
        Fondo.setBounds(0,0,597,418);
        Fondo.setVisible(true);
      
        //Añade los botones
        JButton btnNuevoJuego = new JButton("Nuevo Juego");
        JButton btnEstadisticas = new JButton("Estadisticas");
        JButton btnOpciones = new JButton("Opciones Administrativas");
      
        btnNuevoJuego.setVisible(true);
        btnEstadisticas.setVisible(true);
        btnOpciones.setVisible(true);
      
        btnNuevoJuego.setBounds(205,274,217,30);
        btnEstadisticas.setBounds(205,304,217,26);
        btnOpciones.setBounds(205,330,217,26);
      
        Ventana.add(btnNuevoJuego);
        Ventana.add(btnEstadisticas);
        Ventana.add(btnOpciones);
        Ventana.add(Fondo);
      
      
      
        //Añade ActionListeners a los botones
        btnNuevoJuego.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                NuevoJuego();
            }
        });
      
        btnEstadisticas.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                JOptionPane.showMessageDialog(Ventana, "Temporalemtne no se encuentra esta opción");
            }
        });
      
        btnOpciones.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                JOptionPane.showMessageDialog(Ventana, "Temporalmente no se encuentra esta opción");
            }
        });
    }
  
    private void NuevoJuego(){
        space Juego = new space();
        Juego.Jugar();
    }
  
    public static void main(String args[]){
      
        space Juego = new space();
        Juego.Jugar();
    }

    void setVisible(boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}