package FRAMEBASE;

import java.awt.Image;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Random;

/**
 *@author
 *  Yesenia Lizbeth Guerrero Garcia
 *Nancy Yessenia Garcia Mayorga
 * Yariseth Lizeth Guerrero Garcia
*/
public class Motor {

    private Random NumeroAleatorio = new Random();
    private final int MAXIMA_POSICION_JUGADOR_X = 750;
    private final int MAXIMA_POSICION_JUGADOR_Y = 550;
    public final int NAVES_X = 15;
    public final int NAVES_Y = 5;
    private int JugadorPosicionX = MAXIMA_POSICION_JUGADOR_X / 2;
    private int JugadorPosicionY = MAXIMA_POSICION_JUGADOR_Y;
    private int VelocidadJuego = 0;
    private int TamañoImagen = 50;
    //Variables para el control de las balas
    private ArrayList<Bala> Balas = new ArrayList();
    private int NumeroBalas = 0;
    //Variable para el control de los enemigos
    private Nave[][] MatrizNaves = new Nave[NAVES_Y][NAVES_X];
    private int PulsosBucle = 0;
    private int Pulsos = 0;
    private int CantidadEnemigos = 0;
    //Variables para la deteccion de colisiones
    private Rectangle Imagen1 = new Rectangle();    //Para las naves alienigenas
    private Rectangle Imagen2 = new Rectangle();    //Para las balas del usuario

    public void NuevaBala() {
        Balas.add(new Bala(JugadorPosicionX + 25, JugadorPosicionY));
        NumeroBalas++;
    }

    public Motor() {
        //Establece el tamaño de las balas del jugador
        Imagen2.height = 17;
        Imagen2.width = 7;

        //Establece el tamaño de las naves
        Imagen1.height = 50;
        Imagen1.width = 50;
    }

    public int getNumeroBalas() {
        return NumeroBalas;
    }

    public Bala getBala(int Indice) {
        return Balas.get(Indice);
    }

    public void setTamañoImagen(int Tamaño) {
        this.TamañoImagen = Tamaño;
    }

    public void EstablecerNivel(int Nivel) {
        this.VelocidadJuego = Nivel * 10;
    }

    public int getJugadorPosicionX() {
        return JugadorPosicionX;
    }

    public int getJugadorPosicionY() {
        return JugadorPosicionY;
    }

    public Image getNave(int X, int Y) {
        if (MatrizNaves[Y][X] == null) {
            return null;
        } else {
            return MatrizNaves[Y][X].getImagen();
        }
    }

    public void setJugadorPosicionX(int X) {
        if (X >= 0 && X < MAXIMA_POSICION_JUGADOR_X) {
            this.JugadorPosicionX = X;
        }
    }

    public void setJugadorPosicionY(int Y) {
        if (Y >= 25 && Y < MAXIMA_POSICION_JUGADOR_Y) {
            this.JugadorPosicionY = Y;
        }
    }

    /**
     * ************************************************************
     * Acá genera enemigos aleatorios y crea posiciones aleatorias*
     * ************************************************************
     */
    private int FuncionPulsos(int X) {
        return -10 * X + 60;
    }

    public void setNivel(int Nivel) {
        this.Pulsos = FuncionPulsos(Nivel);
    }

    private void GenerarEnemigo() {
        int X = NumeroAleatorio.nextInt(NAVES_X);
        int Y = NumeroAleatorio.nextInt(NAVES_Y);
        int TipoNave = NumeroAleatorio.nextInt(4);

        //Verifica que esa posición no exista
        while (MatrizNaves[Y][X] != null) {
            X = NumeroAleatorio.nextInt(NAVES_X);
            Y = NumeroAleatorio.nextInt(NAVES_Y);
        }

        MatrizNaves[Y][X] = new Nave(TipoNave);
        CantidadEnemigos++;
    }

    private void DetectarColisiones() {
        //Detecta colisiones entre las balas y los aliens
        for (int i = 0; i < NAVES_X; i++) {
            for (int j = 0; j < NAVES_Y; j++) {
                //Obtiene la posición de la nave alien
                if (MatrizNaves[j][i] != null) {
                    Imagen1.x = 10 + i * TamañoImagen;
                    Imagen1.y = 10 + j * TamañoImagen;

                    //La compara con las posiciones de las balas
                    for (int k = 0; k < Balas.size(); k++) {
                        Imagen2.x = Balas.get(k).getX();
                        Imagen2.y = Balas.get(k).getY();

                        if (Imagen1.intersects(Imagen2)) {
                            if (MatrizNaves[j][i].DisminuirResistencia()) {
                                MatrizNaves[j][i] = null;
                            }
                            Balas.remove(k);
                            NumeroBalas--;
                            break;
                        }
                    }
                }
            }
        }
    }

    public void Jugar() {

        //Mueve un poco las balas
        for (int i = 0; i < NumeroBalas; i++) {
            if (Balas.get(i).MoverBala()) {
                Balas.remove(i);
                NumeroBalas--;
            }
        }

        //Si el tiempo es el correcto crea un nuevo enemigo en una posición aleatoria
        PulsosBucle++;
        if (PulsosBucle == Pulsos) {
            PulsosBucle = 0;
            if (CantidadEnemigos != NAVES_X * NAVES_Y) {
                GenerarEnemigo();
            }
        }

        //Verifica las colisiones
        DetectarColisiones();
    }
}
