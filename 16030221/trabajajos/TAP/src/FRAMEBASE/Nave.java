package FRAMEBASE;

import java.awt.Image;
import javax.swing.ImageIcon;

/**
 *@author
 *  Yesenia Lizbeth Guerrero Garcia
 *Nancy Yessenia Garcia Mayorga
 * Yariseth Lizeth Guerrero Garcia
*/
public class Nave {
    private ImageIcon Imagen = null;
    private int Resistencia = 0;
    
    public Nave(int TipoEnemigo){
        Imagen = new ImageIcon("/FRAMEBASE/navepng" + TipoEnemigo + ".gif");
        switch(TipoEnemigo){
            case 0:{
                Resistencia = 1;
                break;
            }
            case 1:{
                Resistencia = 2;
                break;
            }
            case 2:{
                Resistencia = 3;
                break;
            }
            case 3:{
                Resistencia = 4;
                break;
            }
        }
    }
    
    public Image getImagen(){
        return Imagen.getImage();
    }
    
    public boolean DisminuirResistencia(){   //Devuelve true si su resistencia ha llegado a 0
        Resistencia--;
        if(Resistencia == 0){
            return true;
        }
        else{
            return false;
        }
    }
}
