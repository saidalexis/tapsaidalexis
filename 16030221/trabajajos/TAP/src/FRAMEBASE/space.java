package FRAMEBASE;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
/**
 *@author
 *  Yesenia Lizbeth Guerrero Garcia
 *Nancy Yessenia Garcia Mayorga
 * Yariseth Lizeth Guerrero Garcia
*/
public class space extends JFrame implements KeyListener{

    //Constantes del Juego
    private final int TAMAÑO_NAVE = 50;
    private final int VELOCIDAD_JUGADOR = 3;
    private final ImageIcon Jugador = new ImageIcon("/FRAMEBASE/juagdor.gif");
    private final ImageIcon Bala = new ImageIcon("/FRAMEBASE/bala.png");
    
    //Variables para el movimiento del usuario
    private boolean Arriba = false;
    private boolean Abajo = false;
    private boolean Derecha = false;
    private boolean Izquierda = false;
    
    //Variable para pintar las balas
    private Bala ReferenciaBala;
    
    //Crea el motor del juego
    private Motor Juego = new Motor();
    
    //Variable que controla el juego
    private boolean GameOver = false;
    private boolean Pause = false;
    
    //Doble buffer
    private BufferStrategy DobleBuffer;
    
    
    public space(){
        super(" S P A C E   I N V A D E R S");
        this.setSize(800,600);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        addKeyListener(this);
        createBufferStrategy(2);
        DobleBuffer = getBufferStrategy();
        requestFocus();
    }
    
    public void Jugar(){
        Juego.setNivel(1);
        Juego.setTamañoImagen(TAMAÑO_NAVE);
        BuclePrincipal();
    }
    
    @Override
    public void keyPressed(KeyEvent e) {
        switch(e.getKeyCode()){
            case 37:{       //Izquierda
                Izquierda = true;
                break;
            }
            case 38:{       //Arriba
                Arriba = true;
                break;
            }
            case 39:{       //Derecha
                Derecha = true;
                break;
            }
            case 40:{       //Abajo
                Abajo = true;
                break;
            }
        }
    }
    
    @Override
    public void keyReleased(KeyEvent e) {
        switch(e.getKeyCode()){
            
            case 37:{       //Izquierda
                Izquierda = false;
                break;
            }
            case 38:{       //Arriba
                Arriba = false;
                break;
            }
            case 39:{       //Derecha
                Derecha = false;
                break;
            }
            case 40:{       //Abajo
                Abajo = false;
                break;
            }
            case 65:{       //Letra A
                Juego.NuevaBala();
                break;
            }
        }
    }    
    
    @Override
    public void keyTyped(KeyEvent e) {}
    
    
    
    /*
     * Pinta cada uno de los componentes del juego
     */
    
    private void BuclePrincipal(){
        while(!GameOver){
            ActualizarJuego();
            PintarJuego();
            try{
                Thread.sleep(20);
            }
            catch(Exception Error){System.out.println("ERROR INTERNO DEL JUEGO");}
        }
    }
    
    private void ActualizarJuego(){
        PosicionarJugador();
        Juego.Jugar();
    }
    
    public void PosicionarJugador(){
        if(Izquierda){Juego.setJugadorPosicionX(Juego.getJugadorPosicionX() - VELOCIDAD_JUGADOR);}
        if(Derecha){Juego.setJugadorPosicionX(Juego.getJugadorPosicionX() + VELOCIDAD_JUGADOR);}
        if(Arriba){Juego.setJugadorPosicionY(Juego.getJugadorPosicionY() - VELOCIDAD_JUGADOR);}
        if(Abajo){Juego.setJugadorPosicionY(Juego.getJugadorPosicionY() + VELOCIDAD_JUGADOR);}
    }
    
    private void PintarJuego(){
        //Pinta el Jugador
        paint(getGraphics());
    }
   
    
    @Override
    public void paint(Graphics g){
        g = DobleBuffer.getDrawGraphics();
        g.setColor(this.getBackground());
        g.fillRect(0, 0, getWidth(), getHeight());
        
        //Pinta el Jugador
        g.drawImage(Jugador.getImage(), Juego.getJugadorPosicionX(), Juego.getJugadorPosicionY(), this);
        
        //Pinta las balas
        for(int i = 0; i < Juego.getNumeroBalas(); i++){
            ReferenciaBala = Juego.getBala(i);
            g.drawImage(Bala.getImage(), ReferenciaBala.getX(), ReferenciaBala.getY(), this);
        }
        
        //Pinta las naves
        for(int i = 0; i < Juego.NAVES_X; i++){
            for(int j = 0; j < Juego.NAVES_Y; j++){
                g.drawImage(Juego.getNave(i, j), 10 + i*TAMAÑO_NAVE, 30 + j*TAMAÑO_NAVE, this);
            }
        }
        
        DobleBuffer.show();
        
        
    }
}