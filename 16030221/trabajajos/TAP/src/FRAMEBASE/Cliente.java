package FRAMEBASE;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Scanner;
  
public class Cliente {
  
    public static void main(String[] args) {
  
        //Host del servidor
        final String HOST = "192.168.153.1";
        //Puerto del servidor
        final int PUERTO = 12345;
        DataInputStream entrada;
        DataOutputStream salida;
  
        try {
            while (true) {
            Socket sk = new Socket(HOST, PUERTO);
            
  
            entrada = new DataInputStream(sk.getInputStream());
            salida = new DataOutputStream(sk.getOutputStream());
           
            
            String palabra;
            Scanner sc = new Scanner(System.in);
System.out.print("Mensaje del cliente: ");
palabra  = sc.nextLine();   
 salida.writeUTF(palabra);
            String mensaje = entrada.readUTF();
  
            System.out.println(mensaje);
  //de igual forma con el servidor si dejamos abierta la conexiòn en el cliente el cliente no cierra la conexiòn y sigue en cuestion escuchando 
            
            //sk.close();
           
            }
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
  
    }
  
}