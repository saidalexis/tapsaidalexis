package FRAMEBASE;

/**
 *@author
 *  Yesenia Lizbeth Guerrero Garcia
 *Nancy Yessenia Garcia Mayorga
 * Yariseth Lizeth Guerrero Garcia
*/
public class Bala {

    private final int VELOCIDAD_BALA = 5;
    
    private int PosicionX = 0;
    private int PosicionY = 0;

    public Bala(int PosicionX, int PosicionY) {
        this.PosicionX = PosicionX;
        this.PosicionY = PosicionY;
    }

    public boolean MoverBala(){
        PosicionY = PosicionY - VELOCIDAD_BALA;
        if(PosicionY <= 0){
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public int getX() {
        return PosicionX;
    }

    public int getY() {
        return PosicionY;
    }

    public void setX(int X) {
        this.PosicionX = X;
    }

    public void setY(int Y) {
        this.PosicionY = Y;
    }
}
