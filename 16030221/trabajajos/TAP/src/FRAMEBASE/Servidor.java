package FRAMEBASE;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Scanner;
  
public class Servidor {
  
    public static void main(String[] args) {
  
        ServerSocket servidor = null;
        Socket sk = null;
        DataInputStream entrada;
        DataOutputStream salida;
  
        //puerto de nuestro servidor
        final int PUERTO = 12345;
  
        try {
            //Creamos el socket del servidor
            servidor = new ServerSocket(PUERTO);
            System.out.println("Servidor iniciado");
  
            //Siempre estara escuchando peticiones
            while (true) {
  
                //Espero a que un cliente se conecte
                sk = servidor.accept();
  
                System.out.println("Cliente conectado");
                entrada = new DataInputStream(sk.getInputStream());
                salida = new DataOutputStream(sk.getOutputStream());
  
                //Leo el mensaje que me envia
                String mensaje = entrada.readUTF();
                String palabra;
            Scanner sc = new Scanner(System.in);
System.out.print("Mensaje del servidor: ");
palabra  = sc.nextLine();   
 salida.writeUTF(palabra);
          System.out.println(mensaje);
                //salida.writeUTF("¡Servidor!");
                //Cierro el socket
                
                // al cerrar al socket dejamos abierata la conexiòn con el cliente de una manera para seguie escuchando 
               // sk.close();
                //esto se imprimira por que esta dentro del ciclo descubrir como hacer que se quede con con dic
                //System.out.println("Cliente cerro conexión ");
  
            }
  
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
  
    }
  
}