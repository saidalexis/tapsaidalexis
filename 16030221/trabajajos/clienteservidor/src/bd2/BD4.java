/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd2;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
/**
 *
 * @author likcos
 */
public class BD4 {
      public static void main( String args[] ) {  
    Connection c = null;
      Statement stmt = null;
      try {
         Class.forName("org.postgresql.Driver");
         c = DriverManager
            .getConnection("jdbc:postgresql://localhost:5432/testjava", "postgres", "momo");
         c.setAutoCommit(false);
         System.out.println("Opened database successfully");

         stmt = c.createStatement();
         ResultSet rs = stmt.executeQuery( "SELECT * FROM ALUMNOS;" );
         while ( rs.next() ) {
            int id = rs.getInt("id");
            String  nombre = rs.getString("nombre");
            int edad  = rs.getInt("edad");
            String  calle = rs.getString("calle");
            float adeudo = rs.getFloat("adeudo");
            System.out.println( "ID = " + id );
            System.out.println( "nombre = " + nombre );
            System.out.println( "edad = " + edad );
            System.out.println( "calle = " + calle );
            System.out.println( "salario = " + adeudo );
            System.out.println();
         }
         rs.close();
         stmt.close();
         c.close();
      } catch ( Exception e ) {
         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
         System.exit(0);
      }
      System.out.println("Operation done successfully");
   }
}
