/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd2;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
/**
 *
 * @author likcos
 */
public class BD2 {
  
    public static void main( String args[] ) {
      Connection c = null;
      Statement stmt = null;
      try {
         Class.forName("org.postgresql.Driver");
         c = DriverManager.getConnection("jdbc:postgresql://10.27.12.29:5432/testjava","postgres", "momo");
         System.out.println("Opened database successfully");

         stmt = c.createStatement();
         String sql = "CREATE TABLE jajo " +
            "(ID INT PRIMARY KEY     NOT NULL," +
            " NOMBRE           TEXT    NOT NULL, " +
            " EDAD            INT     NOT NULL, " +
            " CALLE        CHAR(50), " +
            " ADEUDO         REAL)";
            stmt.executeUpdate(sql);
         stmt.close();
         c.close();
      } catch ( Exception e ) {
         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
         System.exit(0);
      }
      System.out.println("Table created successfully");
   }

}
