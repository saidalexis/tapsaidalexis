/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bd2;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
/**
 *
 * @author likcos
 */
public class BD3 {
  public static void main(String args[]) {
      Connection c = null;
      Statement stmt = null;
      try {
         Class.forName("org.postgresql.Driver");
         c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/testdb","postgres", "momo");
         c.setAutoCommit(false);
         System.out.println("Opened database successfully");

         stmt = c.createStatement();
         String sql = "INSERT INTO alumnos (ID,NOMBRE,EDAD,CALLE,ADEUDO) "
            + "VALUES (1, 'Julia', 32, 'Morelos', 20000.00 );";
         stmt.executeUpdate(sql);

         sql = "INSERT INTO alumnos (ID,NOMBRE,EDAD,CALLE,ADEUDO) "
            + "VALUES (2, 'Maria', 25, 'Juarez', 15000.00 );";
         stmt.executeUpdate(sql);

       
         stmt.close();
         c.commit();
         c.close();
      } catch (Exception e) {
         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
         System.exit(0);
      }
      System.out.println("Records created successfully");
   }   
}
