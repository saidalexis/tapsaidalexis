package bd2;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
/**
 *
 * @author likcos
 */
public class BD5 {
     public static void main( String args[] ) {
      Connection c = null;
      Statement stmt = null;
      try {
         Class.forName("org.postgresql.Driver");
         c = DriverManager
            .getConnection("jdbc:postgresql://localhost:5432/testjava","postgres", "momo");
         c.setAutoCommit(false);
         System.out.println("Opened database successfully");

         stmt = c.createStatement();
         String sql = "UPDATE COMPANY set adeudo = 25000.00 where ID=1;";
         stmt.executeUpdate(sql);
         c.commit();

         ResultSet rs = stmt.executeQuery( "SELECT * FROM alumnos;" );
         while ( rs.next() ) {
            int id = rs.getInt("id");
            String  nombre = rs.getString("nombre");
            int edad  = rs.getInt("edad");
            String  calle = rs.getString("calle");
            float adeudo = rs.getFloat("adeudo");
            System.out.println( "ID = " + id );
            System.out.println( "NAME = " + nombre );
            System.out.println( "AGE = " + edad );
            System.out.println( "ADDRESS = " + calle );
            System.out.println( "SALARY = " + adeudo );
            System.out.println();
         }
         rs.close();
         stmt.close();
         c.close();
      } catch ( Exception e ) {
         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
         System.exit(0);
      }
      System.out.println("Operation done successfully");
   }
}
